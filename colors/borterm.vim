hi clear
set background=dark
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "borterm"

hi Comment		ctermfg=8
hi Constant		ctermfg=20
hi String			ctermfg=10
hi Identifier		ctermfg=7					cterm=none
hi Statement		ctermfg=14
hi PreProc		ctermfg=10
hi Type			ctermfg=14
hi Special		ctermfg=14
hi Error			ctermbg=9
hi Todo		 	ctermfg=20	ctermbg=3
hi Directory		ctermfg=10
hi Normal			ctermfg=7		ctermbg=0
hi Search			ctermbg=11
hi Number			ctermfg=11
hi Define			ctermfg=14
hi StatusLine		ctermfg=7		ctermbg=0		cterm=none
hi CursorLine		ctermbg=233				cterm=none
hi VertSplit		ctermfg=7		ctermbg=236	cterm=bold
hi LineNr			ctermfg=3
hi CursorLineNr	ctermfg=11	ctermbg=0 	cterm=none
hi ColorColumn 	ctermfg=7 	ctermbg=234
