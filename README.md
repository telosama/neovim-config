Neovim Config
=============

## What is this?
These are my personal configuration for neovim.

## How to use this config?
This Configuration works best in GNU/Linux platform. Other systems are also supported, but they see less testing, so more bugs will show.
Supported Systems:

- MS Windows, preferably running on MSYS2 Environment (Running on MS Windows directly is untested)
- Android, on Termux
- Theoretically it runs on MacOS and BSDs, but it’s untested, as I don’t have access to any MacOS/BSD system (yet)

### Neovim Version
This configuration can only be used with Neovim version 0.5 and up

### External Dependencies
Some plugins require python extensions for neovim

#### Language Servers


### Installation
1. Download/Clone this git repo to ```$HOME/.config/nvim```
```sh
mkdir -p  $HOME/.config
git clone "https://gitlab.com/telosama/neovim-config.git" $HOME/.config/nvim
```
2. (This is weird, will be fixed) Run Neovim once, ignore the errors
3. Start Neovim Again, then do this command: 
```
:PackerInstall
```
4. Then run this command in Neovim; 
```
:PackerCompile
```
And Done!

## Philosophy
- Just to keep it as simple as possible
- No need for external, complicated plugins, if the need can be satisfied with short, simple, specific personal code.
- These are not rules, they are more of a suggestion/guideline

## Support/Contribution
As can be seen, the author of this repo is a beginner in english and programming, especially in Lua & Git.
So, any constructive help, criticism, contribution is very welcomed.
