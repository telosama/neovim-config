--
-- Configuration Tools
--
local ct = require('conftool')

--
-- Encoding
--
ct.set({'encoding', 'utf-8'}, {'fileencoding', 'utf-8'})

--
-- Search
--
ct.set('wildmenu', 'incsearch', 'hlsearch')
vim.opt.backspace = 'indent,eol,start'

--
-- Mouse
--
vim.opt.mouse = 'a'

--
-- Indentation
--
vim.cmd('filetype plugin indent on')
ct.set('autoindent', 'smartindent', 'cindent')
ct.set({'tabstop', 5}, {'shiftwidth', 5})
-- Indentation Line Indicator for Tabs
vim.opt.list = true
vim.cmd([[set lcs=tab:\|\ ]])

--
-- Folds
--
ct.augroup('AutoSaveFolds', {
		'BufWinLeave,BufLeave,BufWritePost',
		'?*', 'nested silent!',
		'mkview!'
	},{
		'BufWinEnter', '?*', 'silent!', 'loadview'
	}
)

--
-- User Interface
--
ct.set('ruler', 'showcmd', 'cursorline', 'number', 'termguicolors')
ct.set({'signcolumn', 'yes:1'}, {'laststatus', 1}, {'background', 'light'}, {'conceallevel', 0}, {'textwidth', 0}, {'wrapmargin', 0})

vim.cmd('syntax on')
-- dont show line number in terminal window
vim.cmd('autocmd TermOpen * setlocal nonu')

--
-- Misc
--
vim.opt.compatible = false
vim.opt.autowrite = true
-- auto cd to the current file's location
vim.cmd('autocmd BufEnter * silent! lcd %:p:h')
-- highlighting for other langs in markdown docs
vim.g.markdown_fenced_languages = {'html', 'vim', 'python', 'c', 'bash=sh'}

--
-- Spell Checking
--
ct.set({'spelllang', {'en', 'cjk', 'id'}})
ct.set({'spellsuggest', {'best', 5}})

--
-- Plugins
--
require("plugs")

--
-- Filetypes (based settings)
--
require("filetypes")

--
-- Bindings; Maps and Commands
--
require("binds")

