-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

  local time
  local profile_info
  local should_profile = false
  if should_profile then
    local hrtime = vim.loop.hrtime
    profile_info = {}
    time = function(chunk, start)
      if start then
        profile_info[chunk] = hrtime()
      else
        profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
      end
    end
  else
    time = function(chunk, start) end
  end
  
local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end

  _G._packer = _G._packer or {}
  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/iang/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?.lua;/home/iang/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?/init.lua;/home/iang/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?.lua;/home/iang/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/iang/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  ["closetag.vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/closetag.vim",
    url = "https://github.com/alvan/closetag.vim"
  },
  ["coq.artifacts"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/coq.artifacts",
    url = "https://github.com/ms-jpq/coq.artifacts"
  },
  coq_nvim = {
    config = { "\27LJ\2\n=\0\0\3\0\3\0\0056\0\0\0009\0\1\0'\2\2\0B\0\2\1K\0\1\0\30autocmd VimEnter * COQnow\bcmd\bvim\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/coq_nvim",
    url = "https://github.com/ms-jpq/coq_nvim"
  },
  delimitMate = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/delimitMate",
    url = "https://github.com/Raimondi/delimitMate"
  },
  ["goyo.vim"] = {
    commands = { "Goyo" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/goyo.vim",
    url = "https://github.com/junegunn/goyo.vim"
  },
  ["gruvbox.nvim"] = {
    config = { "\27LJ\2\n�\2\0\0\3\0\15\0\0276\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\0016\0\4\0009\0\5\0'\1\a\0=\1\6\0006\0\4\0009\0\b\0'\1\n\0=\1\t\0006\0\4\0009\0\b\0'\1\n\0=\1\v\0006\0\4\0009\0\b\0)\1\1\0=\1\f\0006\0\4\0009\0\r\0'\2\14\0B\0\2\1K\0\1\0\17colo gruvbox\bcmd\19gruvbox_italic\26gruvbox_contrast_hard\thard\27gruvbox_contrast_light\6g\nlight\15background\bopt\bvim\18termguicolors\bset\rconftool\frequire\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/gruvbox.nvim",
    url = "https://github.com/ellisonleao/gruvbox.nvim"
  },
  indentLine = {
    after_files = { "/home/iang/.local/share/nvim/site/pack/packer/opt/indentLine/after/plugin/indentLine.vim" },
    loaded = true,
    needs_bufread = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/indentLine",
    url = "https://github.com/Yggdroot/indentLine"
  },
  ["lualine.nvim"] = {
    config = { "\27LJ\2\n�\2\0\0\5\0\18\0\0256\0\0\0009\0\1\0+\1\1\0=\1\2\0006\0\3\0'\2\4\0B\0\2\0029\0\5\0005\2\6\0B\0\2\0016\0\3\0'\2\a\0B\0\2\0029\0\b\0005\2\14\0005\3\t\0005\4\n\0=\4\v\0035\4\f\0=\4\r\3=\3\15\0025\3\16\0=\3\17\2B\0\2\1K\0\1\0\15extensions\1\3\0\0\14nvim-tree\15toggleterm\foptions\1\0\0\23section_separators\1\0\2\nright\5\tleft\5\25component_separators\1\0\2\nright\5\tleft\5\1\0\1\ntheme\fgruvbox\nsetup\flualine\1\3\0\0\15laststatus\3\2\bset\rconftool\frequire\rshowmode\bopt\bvim\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/lualine.nvim",
    url = "https://github.com/nvim-lualine/lualine.nvim"
  },
  ["nvim-lsp-basics"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/nvim-lsp-basics",
    url = "https://github.com/nanotee/nvim-lsp-basics"
  },
  ["nvim-lspconfig"] = {
    config = { "\27LJ\2\nn\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequire�\b\1\0\18\0I\0�\0016\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0029\3\4\0009\3\5\0039\5\6\0015\a\b\0003\b\a\0=\b\t\aB\5\2\0A\3\0\0019\3\n\0009\3\5\0039\5\6\0015\a\f\0003\b\v\0=\b\t\aB\5\2\0A\3\0\0019\3\r\0009\3\5\0039\5\6\0015\a\15\0003\b\14\0=\b\t\aB\5\2\0A\3\0\0019\3\16\0009\3\5\0039\5\6\0015\a\18\0003\b\17\0=\b\t\aB\5\2\0A\3\0\0019\3\19\0009\3\5\0039\5\6\0015\a\21\0005\b\20\0=\b\22\a3\b\23\0=\b\t\aB\5\2\0A\3\0\0019\3\24\0009\3\5\0039\5\6\0015\a\26\0003\b\25\0=\b\t\aB\5\2\0A\3\0\0019\3\27\0009\3\5\0039\5\6\0015\a\29\0003\b\28\0=\b\t\aB\5\2\0A\3\0\0019\3\30\0009\3\5\0039\5\6\0015\a \0003\b\31\0=\b\t\aB\5\2\0A\3\0\0019\3!\0009\3\5\0039\5\6\0015\a#\0003\b\"\0=\b\t\aB\5\2\0A\3\0\0019\3$\2'\4%\0&\3\4\0036\4&\0009\4'\0049\4(\4\18\6\3\0'\a)\0B\4\3\0026\5&\0009\5*\0056\a+\0009\a,\a'\b-\0B\5\3\0026\6.\0009\6/\6\18\b\5\0'\t0\0B\6\3\0016\6.\0009\6/\6\18\b\5\0'\t1\0B\6\3\0019\0062\0009\6\5\0069\b\6\0015\n5\0005\v3\0>\3\1\v\18\f\4\0'\r4\0&\f\r\f>\f\3\v=\v\22\n3\v6\0=\v\t\n5\vF\0005\f8\0005\r7\0=\5,\r=\r9\f5\r;\0005\14:\0=\14<\r=\r=\f5\rA\0006\14&\0009\14>\0149\14?\14'\16@\0+\17\2\0B\14\3\2=\14B\r=\rC\f5\rD\0=\rE\f=\fG\v=\vH\nB\b\2\0A\6\0\1K\0\1\0\rsettings\bLua\1\0\0\14telemetry\1\0\1\venable\1\14workspace\flibrary\1\0\0\5\26nvim_get_runtime_file\bapi\16diagnostics\fglobals\1\0\0\1\2\0\0\bvim\fruntime\1\0\0\1\0\1\fversion\vLuaJIT\0\1\0\0\14/main.lua\1\3\0\0\0\a-E\16sumneko_lua\19lua/?/init.lua\14lua/?.lua\vinsert\ntable\6;\tpath\fpackage\nsplit\t:p:h\16fnamemodify\afn\bvim:/sumneko_lua/extension/server/bin/lua-language-server\rlsp_path\1\0\0\0\nvimls\1\0\0\0\rtsserver\1\0\0\0\vtexlab\1\0\0\0\fpyright\0\bcmd\1\0\0\1\2\0\0\njdtls\njdtls\1\0\0\0\thtml\1\0\0\0\ngopls\1\0\0\0\vclangd\14on_attach\1\0\0\0\28lsp_ensure_capabilities\nsetup\vbashls\rdefaults\bcoq\14lspconfig\frequire\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/nvim-lspconfig",
    url = "https://github.com/neovim/nvim-lspconfig"
  },
  ["nvim-tree.lua"] = {
    config = { "\27LJ\2\n�\1\0\0\5\0\v\0\0176\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\0016\0\0\0'\2\6\0B\0\2\0029\0\a\0'\2\b\0'\3\t\0'\4\n\0B\0\4\1K\0\1\0\24:NvimTreeToggle<CR>\f<c-S-n>\6n\bmap\rconftool\tview\1\0\1\18disable_netrw\2\1\0\1\nwidth\3\25\nsetup\14nvim-tree\frequire\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/nvim-tree.lua",
    url = "https://github.com/kyazdani42/nvim-tree.lua"
  },
  ["nvim-treesitter"] = {
    config = { "\27LJ\2\n�\1\0\0\4\0\b\0\v6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\2B\0\2\1K\0\1\0\14highlight\1\0\1\venable\2\21ensure_installed\1\0\1\17sync_install\1\1\14\0\0\tbash\vbibtex\6c\bcpp\bcss\ago\thtml\15javascript\tjson\nlatex\vpython\15typescript\bvim\nsetup\28nvim-treesitter.configs\frequire\0" },
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["symbols-outline.nvim"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/symbols-outline.nvim",
    url = "https://github.com/simrat39/symbols-outline.nvim"
  },
  ["vim-css-color"] = {
    loaded = true,
    path = "/home/iang/.local/share/nvim/site/pack/packer/start/vim-css-color",
    url = "https://github.com/ap/vim-css-color"
  },
  ["vim-go"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/vim-go",
    url = "https://github.com/fatih/vim-go"
  },
  ["vim-visual-multi"] = {
    keys = { { "", "<C-Up>" }, { "", "<C-Down>" } },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/vim-visual-multi",
    url = "https://github.com/mg979/vim-visual-multi"
  },
  vimtex = {
    config = { "\27LJ\2\n�\1\0\0\3\0\n\0\0166\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\2\5\0=\2\4\0016\1\2\0009\1\3\0015\2\a\0=\2\6\0016\1\2\0009\1\3\1'\2\t\0=\2\b\1K\0\1\0\bnvr\29vimtex_compiler_progname\1\0\1\6_\r-xelatex$vimtex_compiler_latexmk_engines\23external_pdfviewer\31vimtex_view_general_viewer\6g\bvim\rdefaults\frequire\0" },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/iang/.local/share/nvim/site/pack/packer/opt/vimtex",
    url = "https://github.com/lervag/vimtex"
  }
}

time([[Defining packer_plugins]], false)
-- Setup for: indentLine
time([[Setup for indentLine]], true)
try_loadstring("\27LJ\2\nY\0\0\2\0\5\0\t6\0\0\0009\0\1\0'\1\3\0=\1\2\0006\0\0\0009\0\1\0)\1\0\0=\1\4\0K\0\1\0\26indentLine_setConceal\6|\20indentLine_char\6g\bvim\0", "setup", "indentLine")
time([[Setup for indentLine]], false)
time([[packadd for indentLine]], true)
vim.cmd [[packadd indentLine]]
time([[packadd for indentLine]], false)
-- Config for: gruvbox.nvim
time([[Config for gruvbox.nvim]], true)
try_loadstring("\27LJ\2\n�\2\0\0\3\0\15\0\0276\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\0016\0\4\0009\0\5\0'\1\a\0=\1\6\0006\0\4\0009\0\b\0'\1\n\0=\1\t\0006\0\4\0009\0\b\0'\1\n\0=\1\v\0006\0\4\0009\0\b\0)\1\1\0=\1\f\0006\0\4\0009\0\r\0'\2\14\0B\0\2\1K\0\1\0\17colo gruvbox\bcmd\19gruvbox_italic\26gruvbox_contrast_hard\thard\27gruvbox_contrast_light\6g\nlight\15background\bopt\bvim\18termguicolors\bset\rconftool\frequire\0", "config", "gruvbox.nvim")
time([[Config for gruvbox.nvim]], false)
-- Config for: nvim-tree.lua
time([[Config for nvim-tree.lua]], true)
try_loadstring("\27LJ\2\n�\1\0\0\5\0\v\0\0176\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\0016\0\0\0'\2\6\0B\0\2\0029\0\a\0'\2\b\0'\3\t\0'\4\n\0B\0\4\1K\0\1\0\24:NvimTreeToggle<CR>\f<c-S-n>\6n\bmap\rconftool\tview\1\0\1\18disable_netrw\2\1\0\1\nwidth\3\25\nsetup\14nvim-tree\frequire\0", "config", "nvim-tree.lua")
time([[Config for nvim-tree.lua]], false)
-- Config for: lualine.nvim
time([[Config for lualine.nvim]], true)
try_loadstring("\27LJ\2\n�\2\0\0\5\0\18\0\0256\0\0\0009\0\1\0+\1\1\0=\1\2\0006\0\3\0'\2\4\0B\0\2\0029\0\5\0005\2\6\0B\0\2\0016\0\3\0'\2\a\0B\0\2\0029\0\b\0005\2\14\0005\3\t\0005\4\n\0=\4\v\0035\4\f\0=\4\r\3=\3\15\0025\3\16\0=\3\17\2B\0\2\1K\0\1\0\15extensions\1\3\0\0\14nvim-tree\15toggleterm\foptions\1\0\0\23section_separators\1\0\2\nright\5\tleft\5\25component_separators\1\0\2\nright\5\tleft\5\1\0\1\ntheme\fgruvbox\nsetup\flualine\1\3\0\0\15laststatus\3\2\bset\rconftool\frequire\rshowmode\bopt\bvim\0", "config", "lualine.nvim")
time([[Config for lualine.nvim]], false)
-- Config for: nvim-treesitter
time([[Config for nvim-treesitter]], true)
try_loadstring("\27LJ\2\n�\1\0\0\4\0\b\0\v6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\2B\0\2\1K\0\1\0\14highlight\1\0\1\venable\2\21ensure_installed\1\0\1\17sync_install\1\1\14\0\0\tbash\vbibtex\6c\bcpp\bcss\ago\thtml\15javascript\tjson\nlatex\vpython\15typescript\bvim\nsetup\28nvim-treesitter.configs\frequire\0", "config", "nvim-treesitter")
time([[Config for nvim-treesitter]], false)
-- Config for: nvim-lspconfig
time([[Config for nvim-lspconfig]], true)
try_loadstring("\27LJ\2\nn\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequiren\0\2\a\0\4\0\f6\2\0\0'\4\1\0B\2\2\0029\3\2\2\18\5\0\0\18\6\1\0B\3\3\0019\3\3\2\18\5\0\0\18\6\1\0B\3\3\1K\0\1\0\22make_lsp_mappings\22make_lsp_commands\15lsp_basics\frequire�\b\1\0\18\0I\0�\0016\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0029\3\4\0009\3\5\0039\5\6\0015\a\b\0003\b\a\0=\b\t\aB\5\2\0A\3\0\0019\3\n\0009\3\5\0039\5\6\0015\a\f\0003\b\v\0=\b\t\aB\5\2\0A\3\0\0019\3\r\0009\3\5\0039\5\6\0015\a\15\0003\b\14\0=\b\t\aB\5\2\0A\3\0\0019\3\16\0009\3\5\0039\5\6\0015\a\18\0003\b\17\0=\b\t\aB\5\2\0A\3\0\0019\3\19\0009\3\5\0039\5\6\0015\a\21\0005\b\20\0=\b\22\a3\b\23\0=\b\t\aB\5\2\0A\3\0\0019\3\24\0009\3\5\0039\5\6\0015\a\26\0003\b\25\0=\b\t\aB\5\2\0A\3\0\0019\3\27\0009\3\5\0039\5\6\0015\a\29\0003\b\28\0=\b\t\aB\5\2\0A\3\0\0019\3\30\0009\3\5\0039\5\6\0015\a \0003\b\31\0=\b\t\aB\5\2\0A\3\0\0019\3!\0009\3\5\0039\5\6\0015\a#\0003\b\"\0=\b\t\aB\5\2\0A\3\0\0019\3$\2'\4%\0&\3\4\0036\4&\0009\4'\0049\4(\4\18\6\3\0'\a)\0B\4\3\0026\5&\0009\5*\0056\a+\0009\a,\a'\b-\0B\5\3\0026\6.\0009\6/\6\18\b\5\0'\t0\0B\6\3\0016\6.\0009\6/\6\18\b\5\0'\t1\0B\6\3\0019\0062\0009\6\5\0069\b\6\0015\n5\0005\v3\0>\3\1\v\18\f\4\0'\r4\0&\f\r\f>\f\3\v=\v\22\n3\v6\0=\v\t\n5\vF\0005\f8\0005\r7\0=\5,\r=\r9\f5\r;\0005\14:\0=\14<\r=\r=\f5\rA\0006\14&\0009\14>\0149\14?\14'\16@\0+\17\2\0B\14\3\2=\14B\r=\rC\f5\rD\0=\rE\f=\fG\v=\vH\nB\b\2\0A\6\0\1K\0\1\0\rsettings\bLua\1\0\0\14telemetry\1\0\1\venable\1\14workspace\flibrary\1\0\0\5\26nvim_get_runtime_file\bapi\16diagnostics\fglobals\1\0\0\1\2\0\0\bvim\fruntime\1\0\0\1\0\1\fversion\vLuaJIT\0\1\0\0\14/main.lua\1\3\0\0\0\a-E\16sumneko_lua\19lua/?/init.lua\14lua/?.lua\vinsert\ntable\6;\tpath\fpackage\nsplit\t:p:h\16fnamemodify\afn\bvim:/sumneko_lua/extension/server/bin/lua-language-server\rlsp_path\1\0\0\0\nvimls\1\0\0\0\rtsserver\1\0\0\0\vtexlab\1\0\0\0\fpyright\0\bcmd\1\0\0\1\2\0\0\njdtls\njdtls\1\0\0\0\thtml\1\0\0\0\ngopls\1\0\0\0\vclangd\14on_attach\1\0\0\0\28lsp_ensure_capabilities\nsetup\vbashls\rdefaults\bcoq\14lspconfig\frequire\0", "config", "nvim-lspconfig")
time([[Config for nvim-lspconfig]], false)
-- Config for: coq_nvim
time([[Config for coq_nvim]], true)
try_loadstring("\27LJ\2\n=\0\0\3\0\3\0\0056\0\0\0009\0\1\0'\2\2\0B\0\2\1K\0\1\0\30autocmd VimEnter * COQnow\bcmd\bvim\0", "config", "coq_nvim")
time([[Config for coq_nvim]], false)

-- Command lazy-loads
time([[Defining lazy-load commands]], true)
pcall(vim.cmd, [[command -nargs=* -range -bang -complete=file Goyo lua require("packer.load")({'goyo.vim'}, { cmd = "Goyo", l1 = <line1>, l2 = <line2>, bang = <q-bang>, args = <q-args>, mods = "<mods>" }, _G.packer_plugins)]])
time([[Defining lazy-load commands]], false)

-- Keymap lazy-loads
time([[Defining lazy-load keymaps]], true)
vim.cmd [[noremap <silent> <C-Up> <cmd>lua require("packer.load")({'vim-visual-multi'}, { keys = "<lt>C-Up>", prefix = "" }, _G.packer_plugins)<cr>]]
vim.cmd [[noremap <silent> <C-Down> <cmd>lua require("packer.load")({'vim-visual-multi'}, { keys = "<lt>C-Down>", prefix = "" }, _G.packer_plugins)<cr>]]
time([[Defining lazy-load keymaps]], false)

vim.cmd [[augroup packer_load_aucmds]]
vim.cmd [[au!]]
  -- Filetype lazy-loads
time([[Defining lazy-load filetype autocommands]], true)
vim.cmd [[au FileType go ++once lua require("packer.load")({'vim-go'}, { ft = "go" }, _G.packer_plugins)]]
vim.cmd [[au FileType vue ++once lua require("packer.load")({'closetag.vim'}, { ft = "vue" }, _G.packer_plugins)]]
vim.cmd [[au FileType tex ++once lua require("packer.load")({'vimtex'}, { ft = "tex" }, _G.packer_plugins)]]
vim.cmd [[au FileType html ++once lua require("packer.load")({'closetag.vim'}, { ft = "html" }, _G.packer_plugins)]]
time([[Defining lazy-load filetype autocommands]], false)
vim.cmd("augroup END")
vim.cmd [[augroup filetypedetect]]
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/cls.vim]], true)
vim.cmd [[source /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/cls.vim]]
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/cls.vim]], false)
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tex.vim]], true)
vim.cmd [[source /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tex.vim]]
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tex.vim]], false)
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tikz.vim]], true)
vim.cmd [[source /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tikz.vim]]
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vimtex/ftdetect/tikz.vim]], false)
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vim-go/ftdetect/gofiletype.vim]], true)
vim.cmd [[source /home/iang/.local/share/nvim/site/pack/packer/opt/vim-go/ftdetect/gofiletype.vim]]
time([[Sourcing ftdetect script at: /home/iang/.local/share/nvim/site/pack/packer/opt/vim-go/ftdetect/gofiletype.vim]], false)
vim.cmd("augroup END")
if should_profile then save_profiles() end

end)

if not no_errors then
  error_msg = error_msg:gsub('"', '\\"')
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
