--
-- Configuration Tools
--
local ct = require('conftool')
local defs = require('defaults')

--
-- Maps
--
-- select all
ct.map('n', '<C-a>', '<esc>ggVG<CR>')
-- spawn terminal
ct.map('n', '<C-M-t>', ':!' .. defs.external_terminal .. ' . &<CR><CR>')
-- spwan file manager
ct.map('n', '<C-M-f>', ':!' .. defs.external_filemgr .. ' . &<CR><CR>')
-- spellchecking
ct.map('n', '<C-S>', ':set spell!<CR>')

--
-- Commands
--
ct.defcmd('Xrdb', '!xrdb %')
