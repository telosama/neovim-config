--
-- Configuration Tools
--
local ct = require('conftool')
local defs = require('defaults')
--
-- Templates
--
ct.augroup('templates',
	{'BufNewFile', '*.c', '0r', defs.template_dir .. '/c_stdio.c'},
	{'BufNewFile', '*.cpp', '0r', defs.template_dir .. '/cpp_iostream.cpp'},
	{'BufNewFile', '*.html', '0r', defs.template_dir .. '/html.html'},
	{'BufNewFile', '*.html', '0r', defs.template_dir .. '/html.html'},
	{'BufNewFile', '*.py', '0r', defs.template_dir .. '/py.py'},
	{'BufNewFile', '*.sh', '0r', defs.template_dir .. '/sh.sh'}
)

--
-- FileType based Settings
--
ct.augroup('filetypeSettings',
	{'FileType', 'python',		'setlocal', 'tabstop=4 shiftwidth=4 expandtab'},
	{'FileType', 'html',		'setlocal', 'tabstop=2 shiftwidth=2 expandtab'},
	{'FileType', 'javascript',	'setlocal', 'tabstop=2 shiftwidth=2 expandtab'},
	{'FileType', 'vue',			'setlocal', 'tabstop=2 shiftwidth=2 expandtab'}
)
