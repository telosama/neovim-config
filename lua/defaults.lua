return {
	-- Yes, I use the Cinnamon desktop environment
	template_dir = '~/Templates',
	external_terminal = 'gnome-terminal',
	external_filemgr = 'nemo',
	external_pdfviewer = 'xreader',
	lsp_path = vim.env.HOME .. '/.local/share/lsp_servers'
}
