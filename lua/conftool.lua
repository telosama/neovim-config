local conftool = {}

function conftool.set(...)
	for _,a in ipairs{...} do
		if type(a) == "string" then
			vim.opt[a] = true;
		elseif type(a) == "table" then
			local val = false
			if #a == 2 then val = a[2]
			elseif #a == 1 then val = true end
			vim.opt[ a[1] ] = val
		end
	end
end

function conftool.augroup(name, ...)
	vim.cmd('augroup ' .. name)
	vim.cmd('autocmd!')
	for _, autocmd in ipairs{...} do
		vim.cmd('autocmd ' .. table.concat(autocmd, ' '))
	end
	vim.cmd('augroup END')
end

function conftool.map(mode, keyseq, argcmd, argopts)
	local opts = {noremap = true}
	if argopts then opts = vim.tbl_extend('force', defopts, opts) end
	vim.api.nvim_set_keymap(mode, keyseq, argcmd, opts)
end

function conftool.defcmd(cmd, repl, attr, redef)
	if not redef then redef = '' end
	if not attr then attr = '' end
	vim.cmd('command' .. redef .. ' ' .. cmd  .. ' ' .. attr .. ' ' .. repl)
end

return conftool
