--
-- Built-in Plugins
--

-- Netrw
vim.g.netrw_liststyle = 3
vim.g.netrw_banner = 0
vim.g.netrw_browse_split = 4
vim.g.netrw_winsize = 15
vim.g.netrw_chgwin = 1

--
-- Packer
--

-- Bootstrap Packer
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  Packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

-- Plugins setup
return require('packer').startup(function(use)
	-- let packer manage itself
	use 'wbthomason/packer.nvim'

	--
	-- Simple plugins
	--
	use 'ap/vim-css-color'
	use 'Raimondi/delimitMate'
	use 'simrat39/symbols-outline.nvim'
	use 'nanotee/nvim-lsp-basics'

	--
	-- Plugins with Configurations
	--
	use {
		'kyazdani42/nvim-tree.lua',
		tag = 'nightly',
		requires = {
			'kyazdani42/nvim-web-devicons'
		},
		config = function()
			require('nvim-tree').setup({
				view = {
					width = 25,
				},
				disable_netrw = true,
			})
			require('conftool').map('n', '<c-S-n>', ':NvimTreeToggle<CR>')
		end
	}

	use {
		'ellisonleao/gruvbox.nvim',
		config = function()
			require('conftool').set('termguicolors')
			vim.opt.background = "light"
			vim.g.gruvbox_contrast_light = "hard"
			vim.g.gruvbox_contrast_hard = "hard"
			vim.g.gruvbox_italic = 1
			vim.cmd('colo gruvbox')
		end
	}

	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true},
		config = function()
			vim.opt.showmode = false
			require('conftool').set({"laststatus", 2})
			require('lualine').setup {
				options = {
					theme = 'gruvbox',
					component_separators = { left = '', right = ''},
					section_separators = { left = '', right = ''},
				},
				extensions = {
					"nvim-tree",
					"toggleterm",
				}
			}
		end
	}

	use {
		'junegunn/goyo.vim',
		cmd = 'Goyo'
	}

	use {
		'Yggdroot/indentLine',
		setup = function()
			vim.g.indentLine_char = '|'
			vim.g.indentLine_setConceal = 0
		end
	}

	use {
		'alvan/closetag.vim',
		ft = {"html", "vue"}
	}
	use {
		'fatih/vim-go',
		ft = 'go'
	}

	use {
		'lervag/vimtex',
		ft = 'tex',
		config = function()
			local defs = require 'defaults'
			vim.g.vimtex_view_general_viewer = defs.external_pdfviewer
			vim.g.vimtex_compiler_latexmk_engines = { _ = '-xelatex'}
			vim.g.vimtex_compiler_progname = 'nvr'
		end
	}

	use {
		'mg979/vim-visual-multi',
		keys = {"<C-Up>", "<C-Down>"}
	}

	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate',
		config = function()
			require('nvim-treesitter.configs').setup{
				ensure_installed = {
					"bash",
					"bibtex",
					"c",
					"cpp",
					"css",
					"go",
					"html",
					"javascript",
					"json",
					"latex",
					"python",
					"typescript",
					"vim",
				},
				sync_install = false,
				highlight = {
					enable = true,
				}
			}
		end
	}

	use {
		'ms-jpq/coq_nvim',
		branch = 'coq',
		config = function()
			vim.cmd([[autocmd VimEnter * COQnow]])
		end
	}
	use {
		'ms-jpq/coq.artifacts',
		branch = 'artifacts'
	}


	use {
		'neovim/nvim-lspconfig',
		requires = {'ms-jpq/coq_nvim', 'nanotee/nvim-lsp-basics'},
		config = function()
			local lsp = require 'lspconfig'
			local coq = require 'coq'
			local defs = require 'defaults'

			-- default setups
			lsp.bashls.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.clangd.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.gopls.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.html.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.jdtls.setup(coq.lsp_ensure_capabilities({
				cmd = { 'jdtls' },
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.pyright.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.texlab.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.tsserver.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))
			lsp.vimls.setup(coq.lsp_ensure_capabilities({
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end
			}))

			-- sumneko's lua language server
			local sumneko_bin_path = defs.lsp_path .. '/sumneko_lua/extension/server/bin/lua-language-server'
			local sumneko_root_path = vim.fn.fnamemodify(sumneko_bin_path, ':p:h')
			local runtime_path = vim.split(package.path, ';')
			table.insert(runtime_path, "lua/?.lua")
			table.insert(runtime_path, "lua/?/init.lua")

			lsp.sumneko_lua.setup(coq.lsp_ensure_capabilities({
				cmd = {
					sumneko_bin_path,
					"-E",
					sumneko_root_path .. "/main.lua"
				},
				on_attach = function(client, bufnr)
					local basics = require('lsp_basics')
					basics.make_lsp_commands(client, bufnr)
					basics.make_lsp_mappings(client, bufnr)
				end,
				settings = {
					Lua = {
						runtime = {
							-- Tell the language server,
							-- which version of Lua you're using
							-- (most likely LuaJIT,
							-- in the case of Neovim)
							version = 'LuaJIT',
							-- Setup your lua path
							path = runtime_path,
						},
						diagnostics = {
							-- Get the language server
							-- to recognize the `vim` global
							globals = {'vim'},
						},
						workspace = {
							-- Make the server aware
							-- of Neovim runtime files
							library = vim.api.nvim_get_runtime_file("", true),
						},
						-- Do not send telemetry data
						-- containing a randomized
						-- but unique identifier
						telemetry = {
							enable = false,
						},
					},
				},
			}))
		end
	}


	if Packer_bootstrap then
		require('packer').sync()
	end
end)
